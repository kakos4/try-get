const express = require("express");
const jwt = require("jsonwebtoken");
const jsonParser = express.json();
const app = express();
const PORT = process.env.PORT || 80;

const bcrypt = require('bcrypt');
const saltRounds = 10;

const Sequelize = require("sequelize");
const sequelize = new Sequelize("localTryGetBD", "root", "123", {
  dialect: "mysql",
  host: "localhost",
  define: {
    timestamps: false
  }
});


const User = sequelize.define("user", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      passwordHash: {
        type: Sequelize.STRING,
        allowNull: false
      },
      balance: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      image: {
        type: Sequelize.STRING,
        allowNull: false
      },
      typeAccount: {
        type: Sequelize.STRING,
        allowNull: false
      }
});

const Contract = sequelize.define("contract", {
});

const Game = sequelize.define("game", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
      },
    id_manager: {
        type: Sequelize.INTEGER,
        primaryKey: false,
        allowNull: false
    },
    id_winner: {
        type: Sequelize.INTEGER,
        primaryKey: false,
        allowNull: true
    },
    nameGame: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    },
    enterCost: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    currentCount: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    necessaryCount: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    status: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

User.hasMany(Contract, { onDelete: "cascade" });
Game.hasMany(Contract, { onDelete: "cascade" });
//{force: true}
sequelize.sync().then(result=>{
    app.listen(PORT, ()=>{
      console.log("Server has been started");
  });
}).catch(err=> console.log(err));

app.get('/', (request, response)=>{
    response.json({
      message: "Welcome"
  });
});

//authorization on login window
app.post("/login",jsonParser,(req, res) => {
const userEmail = req.body.email;
const userPassword = req.body.password;
    User.findAll({where:{email: userEmail}, raw: true})
    .then(user=>{
        if(user.length < 1){
          return res.status(401).json({
            message:"Auth failed"
          });
        }
        bcrypt.compare(userPassword, user[0].passwordHash, (err, result)=>{
          if(err){
            return res.status(401).json({
              message:"Auth failed"
            });
          }
          if(result){
            const token = jwt.sign({user}, "secretKey", { expiresIn: '1h' });
            return res.status(200).json({
              message: "Auth successful",
              token: token
            })
          }
          res.status(401).json({
            message: "Auth failed"
          })
      }); 
    }).catch(err=>{
      console.log(err);
      res.status(500).json({error: err});
    });

  });

const imageProfile = "https://e-ga.com.au/cms/wp-content/uploads/2014/03/josh-profile-img1.jpg";
const balanceProfile = 0;

//registration in DB on the registration window
app.post("/create",jsonParser,(req, res, next) => {
  const userName = req.body.name;
  const userPassword = req.body.password;
  const userEmail = req.body.email;

  User.findAll({where:{email: userEmail}, raw: true})
  .then(user=>{
      if(user.length >= 1){
        return res.status(409).json({
          message: "Mail exists"
        });
      } else {
          bcrypt.hash(userPassword, saltRounds, (err,hash)=>{
            if(err){
              return res.status(500).json({error: err});
            } else {
                User.create({
                  name: userName,
                  email: userEmail,
                  passwordHash: hash,
                  balance: balanceProfile,
                  image: imageProfile,
                  typeAccount: "admin"
              }).then(result=>{
                    console.log(result);
                    res.status(201).json({message: "User created!"});
                  }
              ).catch(err=>{
                console.log(err);
                res.status(500).json({error: err});
              });
            }
        });
      }
  })
  .catch()
});

//get data from DB to profile page
app.get("/profile",verifyToken,jsonParser,(req,res)=>{
  jwt.verify(req.token, 'secretKey', (err, authData)=>{
    if(err){
      console.log(err);
      res.sendStatus(403);
    } else{
        if(!req.body) return res.sendStatus(400);
          return res.status(200).json({
            id: authData.user[0].id,
            name: authData.user[0].name,
            email: authData.user[0].email,
            balance: authData.user[0].balance,
            image: authData.user[0].image
          })
    }
  });
});

app.post("/profile", verifyToken, jsonParser, (req, res)=>{
  jwt.verify(req.token, 'secretKey', (err, authData)=>{
    if(err){
      console.log(err);
      res.sendStatus(403);
    } else{
        if(!req.body) return res.sendStatus(400);
          const nameChanged = req.body.name;
          const emailChanged = req.body.email;
          const passwordChanged = req.body.password;
          bcrypt.hash(passwordChanged, saltRounds, (err,hash)=>{
            if(err){
              return res.status(401).json({
                message:"Update failed"
              });
            }
              const userChanged = {
                name: nameChanged,
                email: emailChanged,
                password: hash
              }
                User.update({name: userChanged.name, email: userChanged.email, passwordHash: userChanged.password}, {where: {id: authData.user[0].id}})
              .then(()=>{
                User.findAll({where:{name: userChanged.name}, raw: true})
                .then((user)=>{
                  if(user.length < 1){
                    return res.status(500).json({
                      message: "Fail with findAll function on the Back-end - napishi Romke"
                    });
                  } else{
                    const token = jwt.sign({user}, "secretKey", { expiresIn: '1h' });
                     return res.status(200).json({
                    message: "Successful update. ZAEBUMBA!",
                    token: token
                  });
                }
              })
                .catch(err=>{
                  console.log(err);
                  res.status(500).json({error: err});
                })
              })
              .catch(err=>{
                console.log(err);
                res.status(500).json({error: err});
              })
       });
    }
  });
});


function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}
